<?php

class WordpressException extends Exception
{
    /**
     * @var string
     */
    public $type;

    public function __construct($message, $type, $code, \Exception $previous = null)
    {
        $this->type = $type;
        parent::__construct($message, $code, $previous);
    }

    /**
     * Gets the Exception error type
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}

?>