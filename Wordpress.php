<?php
/**
 * Wordpress SDK
 * A simple PHP SDK for Wordpress API.
 *
 * @package wpcom-php-sdk
 * @author Vince Vervliet <vince@influo.be>
 * @version 0.1
 */

class Wordpress
{
    /**
     * @var string
     */
    private $base_url = 'https://public-api.wordpress.com/rest/v1.1/';

    /**
     * @var string
     */
    private $auth_url = 'https://public-api.wordpress.com/oauth2/authorize';

    /**
     * @var string
     */
    private $token_url = 'https://public-api.wordpress.com/oauth2/token';

    /**
     * @var string
     */
    public $client_id;

    /**
     * @var string
     */
    public $client_secret;

    /**
     * @var string
     */
    public $redirect_uri;

    /**
     * @var string
     */
    public $access_token;

    /**
     * Constructor for the API.
     *
     * @param string $client_id
     * @param string $client_secret
     */
    function __construct($client_id, $client_secret)
    {
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
    }

    /**
     * Set the redirect uri, which is required to get
     * the login url and get the access token.
     *
     * @param $redirect_uri
     */
    public function setRedirectUri($redirect_uri)
    {
        $this->redirect_uri = $redirect_uri;
    }

    /**
     * Set the Access Token.
     *
     * @param string $access_token
     */
    public function setAccessToken($access_token)
    {
        $this->access_token = $access_token;
    }

    /**
     * Return a valid Access Token or the one that is being used by the object.
     *
     * @param string $code
     * @return string
     * @throws Exception
     * @throws WordpressException
     */
    public function getAccessToken($code = null)
    {

        if($code!=""){
            $data = array(
                'client_id' => $this->client_id,
                'client_secret' => $this->client_secret,
                'grant_type' => 'authorization_code',
                'redirect_uri' => $this->redirect_uri,
                'code' => $code
            );


            $result = $this->getTokenCall($this->token_url, $data);

            if(isset($result->error)) {
                throw new Exception($result->error_description);
            }

            $this->access_token = $result;

            return $this->access_token;

        } else if($this->access_token){
            return $this->access_token;
        } else {
            throw new Exception('You must provide the "code" resulting from the login webflow.', 400);
        }
    }

    /**
     * Returns the login URL.
     *
     * @return string
     * @throws Exception
     */
    public function getLoginURL()
    {
        if(!$this->redirect_uri) {
            throw new Exception('You must provide a "redirect_uri".', 400);
        }

        return $this->auth_url.'?client_id='.$this->client_id.'&scope=global&redirect_uri='.$this->redirect_uri.'&response_type=code';
    }



    /**
     * Returns the settings of the current user.
     *
     * @return string
     *
     */
    public function getUserSettings()
    {
        return $this->call("me/settings/");
    }


    /**
     * Returns the sites of the current user.
     *
     * @return string
     *
     */
    public function getUserSites()
    {
        return $this->call("me/sites/");
    }

    /**
     * Return the post of the specified site
     *
     * @param $site
     * @param  $page
     * @param $number
     * @return mixed
     */

    public function getSitePosts($site, $page = 1, $number = 20)
    {
        $data['page'] = $page;
        $data['number'] = $number;
        $query = http_build_query($data);
        $posts = $this->call("/sites/".$site."/posts/?".$query);
        $result = null;

        foreach($posts['posts'] as $postData){
            try {
                $stats = $this->call("/sites/" . $site . "/stats/post/" . $postData['ID']);
            } catch (Exception $e) {
                $stats = null;
            }
            $item['ID'] = $postData['ID'];
            if(isset($postData['post_thumbnail'])) {
                $item['post_thumbnail'] = $postData['post_thumbnail'];
            }
            $item['author'] = $postData['author'];
            $item['title'] = $postData['title'];
            $item['date'] = $postData['date'];
            $item['like_count'] = $postData['like_count'];
            $item['excerpt'] = $postData['excerpt'];
            $item['URL'] = $postData['URL'];
            $item['stats'] = $stats;
            $result[] = $item;
        }

        return $result;
    }


    /**
     * Return the stats of the specified site
     *
     * @param $site
     * @return mixed
     */

    public function getSiteStats($site)
    {
        return $this->call("/sites/".$site."/stats");
    }


    /**
     * Return the url of the specified site
     *
     * @param $site
     * @return mixed
     */

    public function getSiteUrl($site)
    {
        $info = $this->call("/sites/".$site);
        if(!is_null($info)){
            return $info['URL'];
        }
        return null;
    }


    /**
     * Return the stats summary of the specified site
     *
     * @param $site
     * @return mixed
     */

    public function getSiteStatsSummary($site)
    {
        return $this->call("/sites/".$site."/stats/summary");
    }


    /**
     * Return the followers of the specified site
     *
     * @param $site
     * @return mixed
     */

    public function getSiteFollowers($site)
    {
        return $this->call("/sites/".$site."/stats/followers");
    }

    /**
     * Return the country views of the specified site
     *
     * @param $site
     * @return mixed
     */

    public function getSiteCountries($site)
    {
        return $this->call("/sites/".$site."/stats/country-views");
    }


    /**
     * Return the post of the specified site
     *
     * @param $site
     * @return mixed
     *
     */

    public function getSitePost($site, $post)
    {
        $postData = $this->call("/sites/".$site."/posts/".$post);
        try {
            $stats = $this->call("/sites/" . $site . "/stats/post/" . $post);
        } catch (Exception $e) {
            $stats = null;
        }
        $postData['stats'] = $stats;
        return $postData;
    }


    protected function getTokenCall($url, $data){
        $curl = curl_init( $url );
        curl_setopt( $curl, CURLOPT_POST, true );
        curl_setopt( $curl, CURLOPT_POSTFIELDS, $data );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1);
        $auth = curl_exec( $curl );

        $secret = json_decode($auth, true);

        if(isset($secret['error'])){
            throw new Exception($secret['error_description']);
        }
        $access_key = $secret['access_token'];
        return $access_key;
    }

    /**
     * Make both public and authenticated requests to the API.
     *
     * @param string $endpoint
     * @return mixed
     * @throws WordpressException
     */
    public function call($endpoint)
    {
        $url = $this->base_url.trim($endpoint, "/");

        if ($this->access_token) {
            $options = array(
                'http' => array(
                    'ignore_errors' => true,
                    'header' =>
                        array(
                            0 => 'authorization: Bearer '.$this->access_token,
                        ),
                ),
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false
                )
            );

            $context = stream_context_create($options);
            $response = file_get_contents(
                $url,
                false,
                $context
            );
        } else {
            $response = file_get_contents(
                $url,
                false
            );
        }

        $result = json_decode($response, true);

        if (isset($result['error'])) {
            throw new Exception(json_encode($result));
        }

        return $result;
    }
}